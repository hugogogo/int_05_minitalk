# MINITALK

### how to use :

first launch server :

```
./server
```

it will print a line with its own pid, and an empty line below

if you want to terminate the program hit `ctrl-c`

then in another window launch the client, with two arguments :

```
./client [SERVER PID] [string]
```

[SERVER PID] is the pid number printed by the server

[string] is whatever you want to write, surrounded by `"` if you want to include spaces : `"a random phrase"`

### ressources

- **use of getpid** : https://www.includehelp.com/c/getpid-and-getppid-functions-in-c-linux.aspx
- **use of signal** : https://linuxhint.com/signal_handlers_c_programming_language
- **use of sigaction** : https://stackoverflow.com/a/17572787/9497573
- **signal action on sleep** : https://unix.stackexchange.com/questions/351312/sigint-handler-runs-only-once
- **signals are pending just on per type** : https://stackoverflow.com/questions/5285414/signal-queuing-in-c
- **checking for pending signals** : https://www.gnu.org/software/libc/manual/html_node/Checking-for-Pending-Signals.html
- **don't use pause** : https://www.gnu.org/software/libc/manual/html_node/Pause-Problems.html
- **standard signal vs real time signals (with queue option)** : https://www.softprayog.in/programming/posix-real-time-signals-in-linux
- **test time of functions in program** : https://stackoverflow.com/a/5833240/9497573
