
# - - - - - - - - - - - - - - #		name =	value \
#       variables names       #				value
# - - - - - - - - - - - - - - #		! name is case sensitive

VPATH	=	srcs
CC	=	gcc
ODIR	=	./builds
IDIR	=	./includes ./libft/includes
LDIR	=	./libft
LIBS	=	ft
CFLAGS	=	$(IDIR:%=-I%)
CFLAGS	+=	-g3 -Wall -Wextra -Werror
LFLAGS	=	-L./libft -lft

S_NAME	=	server
S_SRCS	=	server.c
S_OBJS	=	$(S_SRCS:%.c=$(ODIR)/%.o)
S_DEPS	=	./includes/server.h

C_NAME	=	client
C_SRCS	=	client.c
C_OBJS	=	$(C_SRCS:%.c=$(ODIR)/%.o)
C_DEPS	=	./includes/client.h


# - - - - - - - - - - - - - - #	target:	prerequisites	| $@ : target
#      rules to execute       #			recipe			| $< : 1st prerequisite
# - - - - - - - - - - - - - - #			recipe			| $^ : all prerequisites

all: $(S_NAME) $(C_NAME)

$(S_NAME): $(S_OBJS) $(S_DEPS)
	make -C $(LDIR)
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS)

$(C_NAME): $(C_OBJS) $(C_DEPS)
	make -C $(LDIR)
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS)

$(ODIR)/%.o: %.c | $(ODIR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(ODIR):
	mkdir $@

leaks: CFLAGS += -fsanitize=address
leaks: clean $(NAME)
#	or:	valgrind --leak-check=full --show-leak-kinds=all ./prgrm
#	or:	valgrind --leak-check=full --leak-resolution=low --show-reachable=yes ./prgrm

clean:
	/bin/rm -f $(S_OBJS) $(C_OBJS)

fclean: clean
	make fclean -C $(LDIR)
	/bin/rm -rf $(ODIR)
	/bin/rm -f $(S_NAME) $(C_NAME)
	/bin/rm -rf a.out a.out.dSYM

libfclean:
	make fclean -C $(LDIR)

re: fclean all

relib: libfclean re

.PHONY: all clean fclean re gcc

