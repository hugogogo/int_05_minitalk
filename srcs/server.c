#include "server.h"

void	send_char(void)
{
	if (g_server.count_bits == 7)
	{
		ft_putchar(g_server.character);
		g_server.count_bits = 0;
		g_server.character = 0;
	}
}

void	sig_handler_1(int sig_num, siginfo_t *info, void *context)
{
	(void)sig_num;
	(void)context;
	g_server.character ^= 1 << (6 - g_server.count_bits);
	g_server.count_bits++;
	send_char();
	kill(info->si_pid, SIGUSR1);
}

void	sig_handler_2(int sig_num, siginfo_t *info, void *context)
{
	(void)sig_num;
	(void)context;
	g_server.count_bits++;
	send_char();
	kill(info->si_pid, SIGUSR1);
}

void	init_message(void)
{
	g_server.count_bits = 0;
	g_server.character = 0;
}

int	main(void)
{
	struct sigaction	act_1;
	struct sigaction	act_2;

	act_1.sa_flags = SA_SIGINFO;
	act_1.sa_sigaction = sig_handler_1;
	act_2.sa_flags = SA_SIGINFO;
	act_2.sa_sigaction = sig_handler_2;
	sigaction(SIGUSR1, &act_1, NULL);
	sigaction(SIGUSR2, &act_2, NULL);
	ft_putnbrendl((int)getpid());
	init_message();
	while (1)
		;
	return (0);
}

/*
** # # # # # # # # # # # # # # # # # # # #
**  INSTRUCTIONS
**
**  allowed functions :
**  - write
**  - signal
**  - sigemptyset
**  - sigaddset
**  - sigaction
**  - kill
**  - getpid
**  - malloc
**  - free
**  - pause
**  - sleep
**  - usleep
**  - exit
**
**  you can only use two signals :
**  - SIGUSR1
**  - SIGUSR2
**
**
** # # # # # # # # # # # # # # # # # # # #
**  UNITS PREFIX
** 
**  times in seconds, deci, centi, milli, micros and nano :
** 
**        c m m
**      d e i i n
**      e n l c a
**      c t l r n
**      i i i o o
** 
**  0 . 0 0 1 0 0
**
**
** # # # # # # # # # # # # # # # # # # # #
**  CODE TO CALCULATE TIME IN MICROSECONDS
** 
**  found here : https://stackoverflow.com/a/5833240/9497573
**
**  #include <stdint.h>
**  #include <sys/time.h>
**  uint64_t micros() {
**  	struct timeval tv;
**  	gettimeofday(&tv,NULL);
**  	return tv.tv_sec*(uint64_t)1000000+tv.tv_usec;
**  }
**  uint64_t t1;
**  uint64_t t2;
**
**
** # # # # # # # # # # # # # # # # # # # #
**  BINARY NUMBERS
** 
**    1 000       0
**    2 001       1 2
**    3 002      10
**    4 003      11 2
**    5 004     100
**    6 005     101
**    7 006     110
**    8 007     111 4
**    9 008    1000
**   10 009    1001
**   11 010    1010
**   12 011    1011
**   13 012    1100
**   14 013    1101
**   15 014    1110
**   16 015    1111 8
**   17 016   10000
**   18 017   10001
**   19 018   10010
**   20 019   10011
**   21 020   10100
**   22 021   10101
**   23 022   10110
**   24 023   10111
**   25 024   11000
**   26 025   11001
**   27 026   11010
**   28 027   11011
**   29 028   11100
**   30 029   11101
**   31 030   11110
**   32 031   11111 16
**   33 032  100000
**   34 033  100001
**   35 034  100010
**   36 035  100011
**   37 036  100100
**   38 037  100101
**   39 038  100110
**   40 039  100111
**   41 040  101000
**   42 041  101001
**   43 042  101010
**   44 043  101011
**   45 044  101100
**   46 045  101101
**   47 046  101110
**   48 047  101111
**   49 048  110000
**   50 049  110001
**   51 050  110010
**   52 051  110011
**   53 052  110100
**   54 053  110101
**   55 054  110110
**   56 055  110111
**   57 056  111000
**   58 057  111001
**   59 058  111010
**   60 059  111011
**   61 060  111100
**   62 061  111101
**   63 062  111110
**   64 063  111111 32
**   65 064 1000000
**   66 065 1000001
**   67 066 1000010
**   68 067 1000011
**   69 068 1000100
**   70 069 1000101
**   71 070 1000110
**   72 071 1000111
**   73 072 1001000
**   74 073 1001001
**   75 074 1001010
**   76 075 1001011
**   77 076 1001100
**   78 077 1001101
**   79 078 1001110
**   80 079 1001111
**   81 080 1010000
**   82 081 1010001
**   83 082 1010010
**   84 083 1010011
**   85 084 1010100
**   86 085 1010101
**   87 086 1010110
**   88 087 1010111
**   89 088 1011000
**   90 089 1011001
**   91 090 1011010
**   92 091 1011011
**   93 092 1011100
**   94 093 1011101
**   95 094 1011110
**   96 095 1011111
**   97 096 1100000
**   98 097 1100001
**   99 098 1100010
**  100 099 1100011
**  101 100 1100100
**  102 101 1100101
**  103 102 1100110
**  104 103 1100111
**  105 104 1101000
**  106 105 1101001
**  107 106 1101010
**  108 107 1101011
**  109 108 1101100
**  110 109 1101101
**  111 110 1101110
**  112 111 1101111
**  113 112 1110000
**  114 113 1110001
**  115 114 1110010
**  116 115 1110011
**  117 116 1110100
**  118 117 1110101
**  119 118 1110110
**  120 119 1110111
**  121 120 1111000
**  122 121 1111001
**  123 122 1111010
**  124 123 1111011
**  125 124 1111100
**  126 125 1111101
**  127 126 1111110
**  128 127 1111111 64
** # # # # # # # # # # # # # # # # # # # #
**
*/
