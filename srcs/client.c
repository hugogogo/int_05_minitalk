#include "client.h"

int	usage(void)
{
	ft_putstr("usage: ./client [server pid] [message]\n");
	return (0);
}

void	send_char(char c, int mask, int server_pid)
{
	if ((c & mask) != 0)
		kill(server_pid, SIGUSR1);
	if ((c & mask) == 0)
		kill(server_pid, SIGUSR2);
}

void	send_message(int sig_num)
{
	(void)sig_num;
	g_client.mask >>= 1;
	if (g_client.mask == 0)
	{
		g_client.mask = 1 << 6;
		if (g_client.text[g_client.count_char] == '\0')
			g_client.done = 1;
		(g_client.count_char)++;
	}
	if (g_client.done == 0)
		send_char(g_client.text[g_client.count_char], g_client.mask, g_client.srv_pid);
}

void	init_client(int pid, char *msg)
{
	g_client.mask = 1 << 7;
	g_client.count_char = 0;
	g_client.srv_pid = pid;
	g_client.text = msg;
	g_client.done = 0;
}

int	main(int ac, char **av)
{
	signal(SIGUSR1, send_message);
	if (ac != 3)
		return (usage());
	init_client(ft_atoi(av[1]), av[2]);
	kill((int)getpid(), SIGUSR1);
	while (g_client.done == 0)
		;
	return (0);
}
