#ifndef CLIENT_H
# define CLIENT_H

# include <signal.h>		// for signal        kill sigaction
# include <sys/types.h>		// for        getpid kill
# include <unistd.h>		// for        getpid
# include "libft.h"

typedef struct s_client
{
	int		mask;
	int		count_char;
	int		srv_pid;
	char	*text;
	int		done;
}			t_client;

t_client	g_client;

#endif

