#ifndef SERVER_H
# define SERVER_H

# include <signal.h>		// for signal        kill sigaction
# include <sys/types.h>		// for        getpid kill
# include <unistd.h>		// for        getpid
# include "libft.h"

typedef struct s_server
{
	unsigned int	count_bits;
	char			character;
}					t_server;

t_server	g_server;

#endif
